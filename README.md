# OCC App List #

A place to list out the various applications we'd like to build that utilize Crypto Coin

### Proposed Apps ###

1. Bill pay (pay your US bills with OCC!)
2. Tic tac crypto (2-player tictactoe using OCC)
3. crytpo shuffle (easily swap any ERC20 for other ERC20, pay fees in OCC)
4. crypto text (text OCC to any US phone number, eventually the world)
5. pacman (buy lives with OCC, win OCC by finding them littered across the game scene)

### Planned Apps ###

1. [Tebak Gambar (a picture guessing game)](https://github.com/mishfit/guessthepicture)
2. QuantBooks ("Quantified Self" data tracking app, pay OCC for reporting features)